/* Copyright (c) 2017, Benoit Chesneau <bchesneau@gmail.com>.
 *
 * This file is part of instrument released under the MIT license.
 * See the NOTICE for more information.
 */

#ifndef ATOMS_H
#define ATOMS_H

namespace instrument {
    extern ERL_NIF_TERM ATOM_OK;
    extern ERL_NIF_TERM ATOM_ERROR;
    extern ERL_NIF_TERM ATOM_EINVAL;
    extern ERL_NIF_TERM ATOM_BADARG;

} // namespace instrument


#endif // ATOMS_H